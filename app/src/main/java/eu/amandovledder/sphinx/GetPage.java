package eu.amandovledder.sphinx;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Amando on 10-4-2017.
 */

public class GetPage {
    public static Models.Page currentPage;

    public GetPage(Models.Page page, Boolean add) {
        System.out.println(MainActivity.PAGES.indexOf(page));
        if(add) {
            currentPage = page;
        } else {
            currentPage = MainActivity.PAGES.get(MainActivity.PAGES.indexOf(page));
        }


        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = PageInfo.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = MainActivity.fragManager;

        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


}
