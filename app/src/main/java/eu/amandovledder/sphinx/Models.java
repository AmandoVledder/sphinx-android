package eu.amandovledder.sphinx;

/**
 * Created by AVledder on 31-3-2017.
 */

public class Models {

    public static class Page {
        public final String id;
        public final String name;
        public final String details;
        public final Boolean isHome;
        public final Boolean isActive;

        public Page(String id, String name, String details, Boolean isHome, Boolean isActive) {
            this.id = id;
            this.name = name;
            this.details = details;
            this.isHome = isHome;
            this.isActive = isActive;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class User {
        public final String id;
        public final String name;
        public final String email;

        public User(String id, String name, String email) {
            this.id = id;
            this.name = name;
            this.email = email;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
