package eu.amandovledder.sphinx;

/**
 * Created by Amando on 3-4-2017.
 */

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class GetPagesTask extends AsyncTask<Void, Void, Boolean> {


    GetPagesTask() {

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean success = false;
        String url = MainActivity.domain + "api?method=getPages";
        try{
            JSONObject jsonObject = JsonRequest.getJSONObjectFromURL(url);
            System.out.println(jsonObject);
            List<Models.Page> temp = new ArrayList<Models.Page>();
            temp = MainActivity.PAGES;
            MainActivity.PAGES.clear();
            String status = jsonObject.getString("status");
            if(status.equals("ok")) {
                success = true;
                String id;
                String name;
                String content;
                String active;
                String home;
                JSONArray array = jsonObject.getJSONArray("data");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject row = array.getJSONObject(i);
                    id = row.getString("id");
                    name = row.getString("name");
                    content = row.getString("content");
                    home = row.getString("homepage");
                    active = row.getString("active");
                    Boolean isHome = false;
                    Boolean isActive = false;

                    if(home.equals("1")) {
                        isHome = true;
                    }

                    if(active.equals("1")) {
                        isActive = true;
                    }

                    System.out.println(name);
                    Models.Page page = new Models.Page(id,name,content,isHome,isActive);
                    MainActivity.PAGES.add(page);

                    //PageFragment.mAdapter.notifyDataSetChanged();
                }
                //PageFragment.mAdapter.notifyDataSetChanged();
            } else {
                success = false;
                MainActivity.PAGES = temp;
                //PageFragment.mAdapter.notifyDataSetChanged();
            }
            System.out.println(status);


        } catch (IOException e) {
            e.printStackTrace();
            //success = false;
        } catch (JSONException e) {
            e.printStackTrace();
            //success = false;
        }
        return success;
    }

    @Override
    protected void onPostExecute(final Boolean success) {


        if (success) {
            PageFragment.mAdapter.notifyDataSetChanged();
        } else {


        }
    }

    @Override
    protected void onCancelled() {

    }
}
