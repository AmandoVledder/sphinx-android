package eu.amandovledder.sphinx;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserInfo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserInfo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserInfo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public UserInfo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserInfo.
     */
    // TODO: Rename and change types and number of parameters
    public static UserInfo newInstance(String param1, String param2) {
        UserInfo fragment = new UserInfo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_user_info, container, false);
        EditText userName = (EditText) view.findViewById(R.id.editUserName);
        userName.setText(GetUser.currentUser.name);
        EditText userEmail = (EditText) view.findViewById(R.id.editUserEmail);
        userEmail.setText(GetUser.currentUser.email);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(GetUser.currentUser.name);

        final Button button = (Button) view.findViewById(R.id.editUserSaveBtn);


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View parent = (View)v.getParent();
                System.out.println("opslaan");
                ////////////////////////////////////////////////
                boolean hasError = false;
                try {
                    EditText userName = (EditText) parent.findViewById(R.id.editUserName);
                    EditText userEmail = (EditText) parent.findViewById(R.id.editUserEmail);
                    RequestParams params = new RequestParams();
                    params.put("name",  userName.getText());
                    params.put("email", userEmail.getText());
                    if(MainActivity.isNew) {
                        EditText userPassword = (EditText) parent.findViewById(R.id.editUserPassword);
                        params.put("password", userPassword.getText());
                        if (!TextUtils.isEmpty(userName.getText()) && !TextUtils.isEmpty(userEmail.getText()) && !TextUtils.isEmpty(userPassword.getText())) {
                            new UpdateUser().AddUser(params);
                            //onButtonPressed(null);
                        } else {
                            if(TextUtils.isEmpty(userName.getText())) {
                                userName.setError("Vul dit veld in.");
                                hasError = true;
                            }
                            if(TextUtils.isEmpty(userEmail.getText())) {
                                userEmail.setError("Vul dit veld in.");
                                hasError = true;
                            }
                            if(TextUtils.isEmpty(userPassword.getText())) {
                                userPassword.setError("Vul dit veld in.");
                                hasError = true;
                            }
                        }
                    } else {
                        params.put("id", GetUser.currentUser.id);
                        if (!TextUtils.isEmpty(userName.getText()) && !TextUtils.isEmpty(userEmail.getText())) {
                            new UpdateUser().UpdateUser(params);
                            //onButtonPressed(null);
                        } else {
                            if(TextUtils.isEmpty(userName.getText())) {
                                userName.setError("Vul dit veld in.");
                                hasError = true;
                            }
                            if(TextUtils.isEmpty(userEmail.getText())) {
                                userEmail.setError("Vul dit veld in.");
                                hasError = true;
                            }
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    if(!hasError) {
                        onButtonPressed(null);
                    }

                }


                ////////////////////////////////////////////////////
            }
        });

        final Button deleteButton = (Button) view.findViewById(R.id.editUserDeleteBtn);
        final EditText passwordField = (EditText) view.findViewById(R.id.editUserPassword);
        final TextView passwordTextView = (TextView) view.findViewById(R.id.passwordTextView);

        if(MainActivity.isNew) {
            deleteButton.setVisibility(View.INVISIBLE);
            passwordField.setVisibility(View.VISIBLE);
            passwordTextView.setVisibility(View.VISIBLE);
        } else {
            deleteButton.setVisibility(View.VISIBLE);
            passwordField.setVisibility(View.INVISIBLE);
            passwordTextView.setVisibility(View.INVISIBLE);
        }

        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View parent = (View)v.getParent();
                System.out.println("opslaan");
                ////////////////////////////////////////////////

                try {

                    RequestParams params = new RequestParams();
                    params.put("id", GetUser.currentUser.id);
                    new UpdateUser().DeleteUser(params);


                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    onButtonPressed(null);
                }


                ////////////////////////////////////////////////////
            }
        });

        final TextView infoTextView = (TextView) view.findViewById(R.id.infoTextView);

        if(GetUser.currentUser.id == "5") {
            deleteButton.setEnabled(false);
            button.setEnabled(false);
            infoTextView.setText("De admin kan niet bewerkt worden.");
            infoTextView.setVisibility(View.VISIBLE);
        } else {
            deleteButton.setEnabled(true);
            button.setEnabled(true);
            infoTextView.setText("");
            infoTextView.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
