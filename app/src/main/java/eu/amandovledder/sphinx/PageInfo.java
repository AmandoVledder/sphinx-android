package eu.amandovledder.sphinx;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.loopj.android.http.*;

import org.json.*;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PageInfo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PageInfo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageInfo extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PageInfo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PageInfo.
     */
    // TODO: Rename and change types and number of parameters
    public static PageInfo newInstance(String param1, String param2) {
        PageInfo fragment = new PageInfo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_page_info, container, false);
        EditText pageName = (EditText) view.findViewById(R.id.editPageName);
        pageName.setText(GetPage.currentPage.name);

        EditText pageAlias = (EditText) view.findViewById(R.id.editPageAlias);
        pageAlias.setText(GetPage.currentPage.name);

        Switch pageActive = (Switch) view.findViewById(R.id.switch3);
        if(GetPage.currentPage.isActive == true) {
            pageActive.setChecked(true);
        } else {
            pageActive.setChecked(false);
        }



        TextView pageContent = (TextView) view.findViewById(R.id.editPageContent);
        pageContent.setText(GetPage.currentPage.details);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(GetPage.currentPage.name);


        final Button button = (Button) view.findViewById(R.id.editPageSaveBtn);


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View parent = (View)v.getParent();
                System.out.println("opslaan");
                ////////////////////////////////////////////////

                try {
                    //new UpdatePage().UpdatePage();
                    EditText pageName = (EditText) parent.findViewById(R.id.editPageName);
                    EditText pageAlias = (EditText) parent.findViewById(R.id.editPageAlias);
                    Switch pageActive = (Switch) parent.findViewById(R.id.switch3);
                    TextView pageContent = (TextView) parent.findViewById(R.id.editPageContent);


                    String isPageActive;
                    if( pageActive.isChecked()) {
                        isPageActive = "1";
                    } else {
                        isPageActive = "0";
                    }
                    RequestParams params = new RequestParams();
                    params.put("name",  pageName.getText());
                    params.put("alias", pageAlias.getText());
                    params.put("content", pageContent.getText());
                    params.put("active", isPageActive);
                    if(MainActivity.isNew) {
                        new UpdatePage().AddPage(params);
                    } else {
                        params.put("id", GetPage.currentPage.id);
                        new UpdatePage().UpdatePage(params);
                    }

                    onButtonPressed(null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                ////////////////////////////////////////////////////
            }
        });

        final Button deleteButton = (Button) view.findViewById(R.id.editPageDeleteBtn);

        if(MainActivity.isNew) {
            deleteButton.setVisibility(View.INVISIBLE);
        } else {
            deleteButton.setVisibility(View.VISIBLE);
        }

        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View parent = (View)v.getParent();
                System.out.println("opslaan");
                ////////////////////////////////////////////////

                try {

                    RequestParams params = new RequestParams();
                        params.put("id", GetPage.currentPage.id);
                        new UpdatePage().DeletePage(params);

                    onButtonPressed(null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                ////////////////////////////////////////////////////
            }
        });

        if(GetPage.currentPage.id == "1") {
            deleteButton.setEnabled(false);
        } else {
            deleteButton.setEnabled(true);
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);



        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
