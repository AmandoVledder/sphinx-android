package eu.amandovledder.sphinx;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Amando on 10-4-2017.
 */

public class GetUser {
    public static Models.User currentUser;

    public GetUser(Models.User user, Boolean add) {
        System.out.println(MainActivity.USERS.indexOf(user));
        if(add) {
            currentUser = user;
        } else {
            currentUser = MainActivity.USERS.get(MainActivity.USERS.indexOf(user));
        }


        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = UserInfo.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = MainActivity.fragManager;

        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


}
