package eu.amandovledder.sphinx;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.amandovledder.sphinx.dummy.DummyContent;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PageFragment.OnListFragmentInteractionListener, PageInfo.OnFragmentInteractionListener, UserFragment.OnListFragmentInteractionListener, UserInfo.OnFragmentInteractionListener {

    public static List<Models.Page> PAGES = new ArrayList<Models.Page>();
    public static List<Models.User> USERS = new ArrayList<Models.User>();
    public static final String PREFS_NAME = "SphinxSettings";
    public static String domain = "";
    public String mDomain = "";
    public static String email = "";
    private GetPagesTask getPagesTask = null;
    private TextView mSideDrawerEmail;
    public static FragmentManager fragManager;
    public static boolean fragmentOpened = false;
    public static ActionBarDrawerToggle toggle;
    public static DrawerLayout drawer;
    public static Toolbar toolbar;
    private Menu menu;
    private static Menu mMenu;
    public static Boolean isNew = false;
    public static Boolean goBackSuper = false;
    public static String activePageTitle = "Pagina's";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = PageFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            getSupportActionBar().setTitle(activePageTitle);
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(true);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean isConfigured = settings.getBoolean("isConfigured", false);

        if(!isConfigured) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            mDomain = settings.getString("domain", "");
            this.domain =  settings.getString("domain", "");
            this.email =  settings.getString("email", "");

            View header=navigationView.getHeaderView(0);
            TextView sideDrawerEmail = (TextView) header.findViewById(R.id.sideDrawerEmail);
            sideDrawerEmail.setText(this.email);
            getPages();
            getUsers();
        }



    }

    public static void setDrawerState(boolean isEnabled) {
        if ( isEnabled ) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();

        }
        else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        System.out.println("KLIK1 "+id);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if(id == R.id.home) {
            getFragmentManager().popBackStackImmediate();
            System.out.println("fattoe");
            onBackPressed();
        } else if(id == R.id.action_add_page) {
            item.setVisible(false);
            addPage();
            isNew = true;
        } else if(id == R.id.action_add_user) {
            item.setVisible(false);
            addUser();
            isNew = true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        System.out.println("KLIK2 "+id);
        if (id == R.id.nav_pages) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = PageFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = MainActivity.fragManager;

            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
            activePageTitle = "Pagina's";
            menu.findItem(R.id.action_add_page).setVisible(true);
            menu.findItem(R.id.action_add_user).setVisible(false);
            getSupportActionBar().setTitle(activePageTitle);
        } else if (id == R.id.nav_users) {
            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = UserFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            FragmentManager fragmentManager = MainActivity.fragManager;

            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(null).commit();
            activePageTitle = "Gebruikers";
            menu.findItem(R.id.action_add_user).setVisible(true);
            menu.findItem(R.id.action_add_page).setVisible(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle(activePageTitle);
        }else if(id == R.id.home) {
            onBackPressed();
        } else if(id == R.id.nav_logout) {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("isConfigured", false);
            editor.apply();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        PageFragment.mAdapter.notifyDataSetChanged();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Models.Page page) {
        System.out.println(page);
    }

    public void getPages() {
        List<Models.Page> temp = new ArrayList<Models.Page>();
        temp = PAGES;

        getPagesTask = new GetPagesTask();
        getPagesTask.execute((Void) null);

        //Models.Page page = new Models.Page("1","Pagina","Welkom",true,true);
        //PAGES.add(page);

        PageFragment.mAdapter.notifyDataSetChanged();
        
    }

    public static void getPage(Models.Page page) {
        new GetPage(page, false);
        fragmentOpened = true;
        mMenu.findItem(R.id.action_add_page).setVisible(false);
        //setDrawerState(false);
    }

    public static void addPage() {
        Models.Page page = new Models.Page("0", "Nieuwe pagina", "", false, false);
        new GetPage(page, true);
    }

    public void getUsers() {
        List<Models.User> temp = new ArrayList<Models.User>();
        temp = USERS;

        GetUsersTask getUsersTask = new GetUsersTask();
        getUsersTask.execute((Void) null);

        //Models.Page page = new Models.Page("1","Pagina","Welkom",true,true);
        //PAGES.add(page);

        UserFragment.mAdapter.notifyDataSetChanged();

    }

    public static void getUser(Models.User user) {
        new GetUser(user, false);
        System.out.println(user);
        fragmentOpened = true;
        mMenu.findItem(R.id.action_add_user).setVisible(false);
        //setDrawerState(false);
    }

    public static void addUser() {
        Models.User user = new Models.User("0", "Nieuwe gebruiker", "");
        new GetUser(user, true);
    }

    public boolean no = false;
    public void goBack(Boolean callOnBack) {
        System.out.println("###" + getSupportFragmentManager().getBackStackEntryCount());
        if(getSupportFragmentManager().getBackStackEntryCount() == 3 && !no) {
            no = true;
            isNew = false;
            onBackPressed();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1 && !no) {
                getFragmentManager().popBackStackImmediate();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setTitle(activePageTitle);
                toggle.syncState();
                setDrawerState(true);
                if (activePageTitle.equals("Gebruikers")) {
                    menu.findItem(R.id.action_add_user).setVisible(true);
                    menu.findItem(R.id.action_add_page).setVisible(false);

                } else {
                    menu.findItem(R.id.action_add_page).setVisible(true);
                    menu.findItem(R.id.action_add_user).setVisible(false);
                }
                isNew = false;
                onBackPressed();
            } else {
                no = false;
                if (callOnBack) {
                    drawer.openDrawer(Gravity.LEFT);
                } else {
                    if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
                        activePageTitle = "Pagina's";
                        getSupportActionBar().setTitle(activePageTitle);
                        menu.findItem(R.id.action_add_page).setVisible(true);
                        menu.findItem(R.id.action_add_user).setVisible(false);
                    } else if(getSupportFragmentManager().getBackStackEntryCount() == 2 && activePageTitle.equals("Gebruikers") ) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                        getSupportActionBar().setHomeButtonEnabled(true);
                    }
                }
                //
            }
        }
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //getFragmentManager().popBackStackImmediate();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            toggle.syncState();
            setDrawerState(true);
            if(activePageTitle.equals("Gebruikers")) {
                menu.findItem(R.id.action_add_user).setVisible(true);
                menu.findItem(R.id.action_add_page).setVisible(false);
                activePageTitle = "Gebruikers";
            } else {
                menu.findItem(R.id.action_add_page).setVisible(true);
                menu.findItem(R.id.action_add_user).setVisible(false);
                activePageTitle = "Pagina's";
            }
            getSupportActionBar().setTitle(activePageTitle);
            super.onBackPressed();
            goBack(false);


        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        goBack(true);
    }

    @Override
    public void onListFragmentInteraction(Models.User user) {

    }
}
