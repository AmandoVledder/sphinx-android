package eu.amandovledder.sphinx;

/**
 * Created by Amando on 3-4-2017.
 */

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GetUsersTask extends AsyncTask<Void, Void, Boolean> {


    GetUsersTask() {

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean success = false;
        String url = MainActivity.domain + "api?method=getUsers";
        try{
            JSONObject jsonObject = JsonRequest.getJSONObjectFromURL(url);
            System.out.println(jsonObject);
            List<Models.User> temp = new ArrayList<Models.User>();
            temp = MainActivity.USERS;
            MainActivity.USERS.clear();
            String status = jsonObject.getString("status");
            if(status.equals("ok")) {
                success = true;
                String id;
                String name;
                String email;

                JSONArray array = jsonObject.getJSONArray("data");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject row = array.getJSONObject(i);
                    id = row.getString("id");
                    name = row.getString("name");
                    email = row.getString("email");


                    System.out.println(name);
                    Models.User user = new Models.User(id,name,email);
                    MainActivity.USERS.add(user);

                    //PageFragment.mAdapter.notifyDataSetChanged();
                }
                //PageFragment.mAdapter.notifyDataSetChanged();
            } else {
                success = false;
                MainActivity.USERS = temp;
                //PageFragment.mAdapter.notifyDataSetChanged();
            }
            System.out.println(status);


        } catch (IOException e) {
            e.printStackTrace();
            //success = false;
        } catch (JSONException e) {
            e.printStackTrace();
            //success = false;
        }
        return success;
    }

    @Override
    protected void onPostExecute(final Boolean success) {


        if (success) {
            UserFragment.mAdapter.notifyDataSetChanged();
        } else {


        }
    }

    @Override
    protected void onCancelled() {

    }
}
