package eu.amandovledder.sphinx;

/**
 * Created by Amando on 30-4-2017.
 */

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

class UpdateUser {
    public void UpdateUser(RequestParams params) throws JSONException {

        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=updateUser", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetUsersTask getUsersTask = new GetUsersTask();
                getUsersTask.execute((Void) null);

                //Models.User page = new Models.User("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                UserFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }

    public void AddUser(RequestParams params) throws JSONException {
        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=addUser", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetUsersTask getUsersTask = new GetUsersTask();
                getUsersTask.execute((Void) null);

                //Models.User page = new Models.User("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                UserFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }

    public void DeleteUser(RequestParams params) throws JSONException {
        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=deleteUser", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetUsersTask getUsersTask = new GetUsersTask();
                getUsersTask.execute((Void) null);

                //Models.User page = new Models.User("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                UserFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }
}