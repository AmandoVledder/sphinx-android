package eu.amandovledder.sphinx;

/**
 * Created by Amando on 30-4-2017.
 */

import android.support.v7.app.AppCompatActivity;

import org.json.*;
import com.loopj.android.http.*;
import cz.msebera.android.httpclient.Header;

class UpdatePage {
    public void UpdatePage(RequestParams params) throws JSONException {

        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=updatePage", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetPagesTask getPagesTask = new GetPagesTask();
                getPagesTask.execute((Void) null);

                //Models.Page page = new Models.Page("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                PageFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }

    public void AddPage(RequestParams params) throws JSONException {
        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=addPage", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetPagesTask getPagesTask = new GetPagesTask();
                getPagesTask.execute((Void) null);

                //Models.Page page = new Models.Page("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                PageFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }

    public void DeletePage(RequestParams params) throws JSONException {
        //post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
        SphinxApiClient.post("api?method=deletePage", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                System.out.println(response);
                GetPagesTask getPagesTask = new GetPagesTask();
                getPagesTask.execute((Void) null);

                //Models.Page page = new Models.Page("1","Pagina","Welkom",true,true);
                //PAGES.add(page);

                PageFragment.mAdapter.notifyDataSetChanged();

            }
        });
    }
}